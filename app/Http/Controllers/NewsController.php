<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;
use Auth;
use App\User;
use App\News;
use App\UserService;
use App\Helpers\ItfVideo;
use App\TagInformation;
use App\TagRelation;

class NewsController extends Controller
{
    public function __construct(){
      parent::__construct();
  }


  public function index() {

   $data=array();
   $data["user_service_data"]=UserService::where(array("user_id"=>Auth::user()->id,"service_id"=>"2"))->firstOrFail();
   $data["alldata"]=News::where("user_id",Auth::user()->id)->orderby("created_at","desc")->paginate(10);
   return view('news.index', compact('data'));

}

public function create(Request $request) {

   return view('news.create');
}

public function show(Request $request,$id) {
    $data=array();
    $data["details"]=News::where(array("id"=>$id,"user_id"=>Auth::user()->id))->first();
    return view('news.show', compact('data'));
}


public function store(Request $request) {

  $data = $request->all();

  $arrTag = array();
     if(!empty($data['tag'])){
        $arrTag = explode(",", $data['tag']);
     }

  $rules = array(
    'title' => 'required',
    );
  $validator = Validator::make($data, $rules);
  if ($validator->fails()){
      return response()->json(array("status"=>"failed","message"=>"Please fill the form","errors"=>$validator->errors()));
  }
  else {

    $newsdata = $request->only('title','description');
    $newsdata["user_id"]=Auth::user()->id;            
    $newsfind = new News();
    $newsfind->fill($newsdata);
    $newsfind->save();

    foreach ($arrTag as $value) {
        $tagData['content_id'] = $newsfind->id;
        $tagData['content_type'] = 'N';
        $tagData['tag_informations_id'] = $value;
        $tagsave = new TagRelation();
        $tagsave->fill($tagData);
        $tagsave->save();
    }
    $redirects=route('news.index');
    return response()->json(array("status"=>"success","redirect"=>$redirects));
}

}

public function edit(Request $request,$id) {

    $newsinfo = News::where(array("id"=>$id,"user_id"=>Auth::user()->id))->firstOrFail(); 
    return view('news.edit',compact("newsinfo"));
}


public function update(Request $request,$id) {


   $data = $request->all();
   $arrTag = array();
     if(!empty($data['tag'])){
        $arrTag = explode(",", $data['tag']);
     }

   $rules = array(
    'title' => 'required',
    );
   $validator = Validator::make($data, $rules);
   if ($validator->fails()){
      return response()->json(array("status"=>"failed","message"=>"Please full the form","errors"=>$validator->errors()));
  }
  else {

    $newsdata = $request->only('title','description');

    $newsfind = News::where(array("id"=>$id,"user_id"=>Auth::user()->id))->firstOrFail();             
    $newsfind->fill($newsdata);
    $newsfind->save();
    $deleteTags = TagRelation::where(array("content_id"=>$id,"content_type"=>"N"));
    $deleteTags->delete();

    foreach ($arrTag as $value) {
        $tagData['content_id'] = $id;
        $tagData['content_type'] = 'N';
        $tagData['tag_informations_id'] = $value;
        $tagsave = new TagRelation();
        $tagsave->fill($tagData);
        $tagsave->save();
    }
    $redirects=route('news.index');
    return response()->json(array("status"=>"success","redirect"=>$redirects));
}

}


public function destroy(Request $request,$photoid){

   $photoinfo = News::where(array("id"=>$photoid,"user_id"=>Auth::user()->id))->firstOrFail();    	

   if(isset($photoinfo->id)){
    $photoinfo->delete();
    return redirect()->route("news.index")->with("message","News has been deleted successfull");
}else{
    return redirect()->back()->with("errormessage","News has deletion failed.");
}

echo $photoid; die;

}

public static function latestNews($limit="1"){
    $data=array();
    $data["latest_news"]=News::where("user_id",Auth::user()->id)->orderby("created_at","desc")->paginate($limit);
    return view('news.latest_news', compact('data'));
}

public static function UserlatestNews($username,$limit="1"){
    $data=array();
    $data["userinformation"] = User::where(array('username'=>$username,"status"=>"1","user_type"=>"N"))->firstOrFail();
    $data["latest_news"]=News::where("user_id",$data["userinformation"]->id)->orderby("created_at","desc")->paginate($limit);
    return view('news.user_latest_news', compact('data'));
}

public function userNews(Request $request,$username) {

    $data=array();
    $data["userinformation"] = User::where(array('username'=>$username,"status"=>"1","user_type"=>"N"))->firstOrFail();
    $data["alldata"]=News::where(array("user_id"=>$data["userinformation"]->id,"status"=>"1"))->orderby("created_at","desc")->paginate(10);
    return view('news.userindex', compact('data'));

}

public function details(Request $request,$username,$id) {
    ItfVideo::views($request, $id,"N");
    $data=array();
    $data["userinformation"] = User::where(array('username'=>$username,"status"=>"1","user_type"=>"N"))->firstOrFail();
    $data["details"]=News::where(array("id"=>$id,"user_id"=>$data["userinformation"]->id))->first();
    return view('news.details', compact('data'));
}

public function publish(Request $request,$id){
    $details=News::where(array("id"=>$id))->first(); 
    if($details->status==1)
        $newstatus=0;
    else 
        $newstatus=1;

    $newsfind = News::where(array("id"=>$id,"user_id"=>Auth::user()->id))->firstOrFail();             
    if($newsfind) $newsfind->status = $newstatus;
    $newsfind->save();
    return redirect()->route("news.index")->with("message","News has been updated successfull");    
}


    // admin modules

public function adminIndex(Request $request) {
    
    /*if(!empty($search_string['search_text'])){
        $data['search_text'] = $search_string['search_text'];
        $newsinfo =News::where('title','like','%'.$search_string['search_text'].'%')
        ->orwhere('description','like','%'.$search_string['search_text'].'%')
        ->orderby("created_at","desc")->paginate(10);
    }else{
        $newsinfo =News::orderby("created_at","desc")->paginate(10);
    }*/

      $search_string = $request->all();
        $data=array();
        $query = News::orderby("created_at","desc");

        if(!empty($search_string['search_text'])){
            $data['search_text'] = $search_string['search_text'];
            $query->whereRaw("(title like '%".$search_string['search_text']."%' OR description LIKE '%".$search_string['search_text']."%') ");
        }
        if(!empty($search_string['daterange'])){
            $data['daterange'] = $search_string['daterange'];
            $date_split = explode(' to ',$data['daterange']);
            $from_date = $data['from_date'] = isset($date_split[0])?$date_split[0]:"";
            $to_date = $data['to_date'] = isset($date_split[1])?$date_split[1]:"";

           $query->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d') >= '$from_date'")->whereRaw("DATE_FORMAT(created_at,'%Y-%m-%d')<= '$to_date'");
        }
        $newsinfo = $query->paginate(10);

    return view('news.admin.index', compact('newsinfo','data'));
}

public function adminUserIndex(Request $request,$username) {
    $search_string = $request->all();
    $data=array();
    $data["userinformation"] = User::where(array('username'=>$username,"status"=>"1","user_type"=>"N"))->firstOrFail();
    if(!empty($search_string['search_text'])){
        $data['search_text'] = $search_string['search_text'];
        $newsinfo =News::where("user_id",$data["userinformation"]->id)
        ->where('title','like','%'.$search_string['search_text'].'%')
        ->orwhere('description','like','%'.$search_string['search_text'].'%')
        ->orderby("created_at","desc")->paginate(10);
    }else{
        $newsinfo =News::where("user_id",$data["userinformation"]->id)->orderby("created_at","desc")->paginate(10);
    }
    return view('news.admin.userindex', compact('newsinfo','data'));
}


public function adminDetails(Request $request,$id) {
    $data=array();
    $data["details"]=News::where(array("id"=>$id))->first();
    return view('news.admin.details', compact('data'));
}

public function adminDelete(Request $request,$id)
{
    $catinfo = News::where(array("id"=>$id))->firstOrFail();

    if(isset($catinfo->id)){
        $catinfo->delete();
        return redirect()->route("admin.news")->with("message","News has been deleted successfull");
    }else{
        return redirect()->back()->with("errormessage","News deletion has been failed.");
    }
}

public function adminStatus(Request $request,$id){
    $details=News::where(array("id"=>$id))->first(); 
    if($details->status==1)
        $newstatus=0;
    else 
        $newstatus=1;

    $newsfind = News::where(array("id"=>$id))->firstOrFail();             
    if($newsfind) $newsfind->status = $newstatus;
    $newsfind->save();
    return redirect()->route("admin.news")->with("message","Articles has been updated successfull");
}

public static function newsWidget($limit="5"){
    $data=array();
    $data["latest_news"]=News::orderby("created_at","desc")->paginate($limit);
    return view('news.admin.news_widget', compact('data'));
}
}
